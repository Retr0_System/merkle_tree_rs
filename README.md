# Merkle Tree на Rust

## Постановка задачи: 

```
Реализовать контейнер (тип данных) Merkle Tree на языке программирования Rust в виде самостоятельной библиотеки.

Реализация должна обладать такими свойствами:
- код должен быть организован в виде “крейта” (так называются пакеты/библиотеки/модули в Rust);
- библиотека должна быть минимально задокументирована и протестирована;
- библиотека должна обладать прагматичным интерфейсом (API). Использование библиотеки должно быть гибким,
    универсальным и соответствовать принципам, принятым в сообществе Rust (посмотрите на стандартные реализации алгоритмов 
    в стандартной библиотеке std::collections)
- автор не должен “изобретать велосипед“, и должен по максимуму использовать существующие проверенные решения 
    (не нужно пытаться самостоятельно реализовывать алгоритмы криптографического хеширования); 
- реализация библиотеки должна быть эффективной (но без фанатизма и экономии наносекунд ценой нечитаемости кода). 
    Для проверки эффективности можно использовать бенчмарки (cм. Rust Book); 
- реализация библиотеки должна соответствовать общепринятым практикам коммерческой разработки (например, не нужно 
    называть переменные однобуквенными именами);
- результатом тестового задания является:
    - Исходный код проекта
    - Текстовый документ, в котором кратко описывается ход мыслей автора, обосновываются принятые технические решения 
        и указываются текущие недостатки реализации; 
    - все это нужно выложить на github или bitbucket и предоставить ссылку для оценки выполненного заданияПолезные ссылки:
        - Rust Book: https://doc.rust-lang.org/book/- 
        - Rust Book на русском: http://rurust.github.io/rust_book_ru/
        - Стандартная библиотека Rust: https://doc.rust-lang.org/std/
        - Советы по написанию библиотек на Rust: https://pascalhertleif.de/artikel/good-practices-for-writing-rust-libraries/
        - Тоже самое, только на русском: https://habrahabr.ru/post/280482/
        - Merkle Tree (wiki): https://en.wikipedia.org/wiki/Merkle_tree
        - Merkle Tree в документации Bitcoin: https://en.bitcoin.it/wiki/Protocol_documentation#Merkle_Trees
Если у вас возникнут трудности или вам нужно будет получить совет, можно не стесняясь обращаться:
    - К русскоязычному сообществу Rust (https://gitter.im/ruRust/general), которое может помочь с любыми вопросами, 
        касающимися использования Rust.
```

## Технические решения

## Итерация мыслей 0

Если с большинством пунктов задачи всё достаточно просто, то с третьим пунктом не очень.
Каким должен быть прагматичный, гибкий, универсальный и идиоматичный API?
Для того, чтобы ответить на этот вопрос я решил прочесть материалы, которые нашёл по этой структуре данных.
Было выяснено, что у Merkle Tree нет как такового "стандарта" реализации, следовательно:

1) Тип используемой хэш-функции может быть отличным от SHA-2. Делаем вывод, что пользователь должен 
    иметь способ при создании экземпляра этой структуры выбрать необходимую ему хэш-функцию. В Rust, есть библиотека 
    для криптографии решающая данный вопрос - это [rust-crypto](https://github.com/DaGenix/rust-crypto/). 
    Типаж [Digest](https://docs.rs/rust-crypto/0.2.36/crypto/digest/trait.Digest.html) реализован для наибольшего 
    количества хэш-функций, включая SHA-1,SHA-2,MD5 и других, поэтому было решено использовать его.
    
2) Тип данных "листьев" дерева в котором они будут отображены перед непосредственно хэшированием не известен, 
    так же заранее не известно каким типом данных будет располагать пользователь. Это могут быть не только строки 
    (как это обычно делают в реализациях/примерах Merkle Tree), но и примитивные типы, кастомные структуры, коллекции. 
    Следовательно необходимо иметь какой-то типаж, который позволит на стороне пользователя указывать, что его данные 
    сериализуемы и желательно, если эти данные представлены структурой, выводить код для сериализации автоматически.
    Сообществом для этих целей была разработана библиотека [Serde](https://serde.rs/). Типаж [Serialize](https://docs.serde.rs/serde/trait.Serialize.html) 
    позволит передавать пользователю любые типы, которые его реализуют. Типаж [Serializer](https://docs.rs/serde/1.0.8/serde/ser/trait.Serializer.html) 
    позволит передавать пользователю сериализатор нужного ему формата данных.
    
3) Пользователь должен иметь возможность: инициализировать пустое дерево, для этого будет использоваться типаж 
    [Default](https://doc.rust-lang.org/std/default/trait.Default.html) из стандартной библиотеки; 
    инициализировать дерево на основе нескольких "листьев"; и на одном "листке".
    При этом функция инициализации должна принимать как векторы, так и массивы - явный признак того, что в сигнатуре 
    функции, в качестве аргумента следует использовать слайс (он абстрагирует нас от конкретной коллекции).
    
## Итерация мыслей 1

1) Была предпринята попытка реализовать функцию
    
    
## Использовавшиеся материалы:
- [Статья о Merkle Tree на wikipedia](https://en.wikipedia.org/wiki/Merkle_tree)
- [7-ая глава книги Mastering Bitcoin](http://chimera.labs.oreilly.com/books/1234000001802/ch07.html#merkle_trees)
- [Страница в глоссарии на Bitcoin.org](https://bitcoin.org/en/glossary/merkle-tree)
- [Статья в bitcoin developer guide](https://bitcoin.org/en/developer-guide#transaction-data)
- [Merkle Signature Schemes, Merkle Trees and Their Cryptanalysis](https://www.emsec.rub.de/media/crypto/attachments/files/2011/04/becker_1.pdf)
- [Tree Hash EXchange format (THEX)](http://adc.sourceforge.net/draft-jchapweske-thex-02.html#anchor2)
- [Building a Merkle Tree in C++](https://codetrips.com/2016/06/19/implementing-a-merkle-tree-in-c/)
- [Understanding merkle trees](https://www.codeproject.com/Articles/1176140/Understanding-Merkle-Trees-Why-use-them-who-uses-t)
- [Подпись Меркла](https://ru.wikipedia.org/wiki/%D0%9F%D0%BE%D0%B4%D0%BF%D0%B8%D1%81%D1%8C_%D0%9C%D0%B5%D1%80%D0%BA%D0%BB%D0%B0)
- [What is MTP (Merkle Tree Proof) and why is it an ideal Proof of Work algorithm?](https://github.com/zcoinofficial/zcoin/wiki/What-is-MTP-(Merkle-Tree-Proof)-and-why-is-it-an-ideal-Proof-of-Work-algorithm%3F)
- [What is a merkle tree](https://www.weusecoins.com/what-is-a-merkle-tree/)
- [Patricia Tree](https://github.com/ethereum/wiki/wiki/Patricia-Tree)