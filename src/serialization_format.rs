use bincode;
use serde_json;
use rmp_serde;
use std::default::Default;
use std::fmt::Debug;
use serde::Serialize;

#[derive(Debug, Eq, PartialEq)]
pub enum SerializationFormat {
    Json,
    MsgPack,
    Bincode,
}

impl SerializationFormat {
    pub fn serialize<SerializableType>(&self, value: &SerializableType) -> Vec<u8>
        where SerializableType: Serialize
    {
        match self {
            &SerializationFormat::Json => serde_json::to_string(&value).unwrap().into_bytes(),
            &SerializationFormat::MsgPack => rmp_serde::to_vec(&value).unwrap(),
            &SerializationFormat::Bincode => bincode::serialize(&value, bincode::Infinite).unwrap(),
        }
    }
}
