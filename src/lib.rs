#![feature(option_entry)]

#[macro_use]
extern crate log;
extern crate crypto;
extern crate bincode;
extern crate serde;
extern crate hex_slice;
extern crate serde_json;
extern crate rmp_serde;
#[macro_use]
extern crate serde_derive;
extern crate rayon;

mod merkle_tree;
mod serialization_format;
pub mod hash_function;

pub use serialization_format::SerializationFormat;
pub use merkle_tree::MerkleTree;
