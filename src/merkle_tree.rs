use serde::Serialize;
use std::default::Default;
use std::rc::Rc;
use std::fmt::Debug;
use std::marker::PhantomData;
use std::fmt::{Display, Formatter, Result};
use serialization_format::SerializationFormat;
use rayon::prelude::*;
use rayon;
use hash_function::{hash_leaf, hash_node};
use std;

#[derive(Debug, Eq, PartialEq)]
pub struct MerkleTree {
    pub layers: Vec<Vec<[u8; 32]>>,
    pub leafs_count: u64,
    pub format: SerializationFormat,
    pub parallel: bool,
    pub builded: bool,
}

impl Default for MerkleTree {
    fn default() -> MerkleTree {
        let mut layers = Vec::with_capacity(256);
        layers.push(Vec::with_capacity(2500));
        MerkleTree {
            layers: layers,
            leafs_count: 0,
            parallel: true,
            builded: false,
            format: SerializationFormat::MsgPack,
        }
    }
}

impl MerkleTree {
    pub fn build(&mut self) {
        match self.leafs_count {
            0 => {
                panic!("No leafs in tree!");
            }
            1 => {
                debug!("Tree have one leaf. Merke root hash == hash(leaf[0])");
                let hashed_leaf = hash_leaf(&self.layers[0][0]);
                debug!("Layers len: {}", self.layers.len());
                self.layers.push(Vec::with_capacity(1250));
                self.layers[1].push(hashed_leaf);
            }
            _ => {
                debug!("Tree have more one leaf.");
                self.recursive_create_nodes(0);
            }
        }
        self.builded = true;
    }

    pub fn from<Serializable>(leafs: &mut [Serializable], format: SerializationFormat) -> MerkleTree where Serializable: Serialize + Clone {
        let log2_leafs = (leafs.len() as f64).log2();
        println!("log2 leafs: {}", log2_leafs);
        let mut base_layer = leafs.iter().map(|element| {
            let serialized_value = format.serialize(&element);
            hash_leaf(&serialized_value)
        }).collect::<Vec<[u8;32]>>();
        let mut layer_len = base_layer.len();
        let base_layer_len = layer_len;
        if layer_len % 2 != 0 {
            let last_leaf = base_layer.remove(layer_len - 1);
            base_layer.push(last_leaf.clone());
            base_layer.push(last_leaf);
        }
        let mut layer_len = base_layer.len();
        base_layer.reserve(layer_len);
        let mut layers : Vec<Vec<[u8; 32]>> = Vec::with_capacity(((log2_leafs+1.0).round()) as usize);
        layers.push(Vec::with_capacity(layer_len));
        while layer_len != 1 {
            debug!("Create new layer with capacity: {}", layer_len);
            layers.push(Vec::with_capacity(layer_len));
            layer_len = layer_len/2;
        }
        layers[0].append(&mut base_layer);
        MerkleTree {
            layers: layers,
            leafs_count: base_layer_len as u64,
            parallel: true,
            builded: false,
            format: SerializationFormat::Bincode,
        }

    }

    fn recursive_create_nodes(&mut self, current_layer_index: usize) {
        debug!("Layer index: {}", current_layer_index);
        if self.layers[current_layer_index].len() > 1 {
            self.add_odd_duplicate(current_layer_index);
            if self.parallel {
                self.generate_new_layer_parallel(current_layer_index);
            } else {
                self.generate_new_layer_sequence(current_layer_index);
            }
            self.recursive_create_nodes(current_layer_index + 1);
        }
    }

    fn add_odd_duplicate(&mut self, current_layer_index: usize) {
        let current_layer_len = self.layers[current_layer_index].len();
        if current_layer_len % 2 != 0 {
            debug!("Список нечётный - добавляем дубликат последнего элемента в список.");
            let last_leaf = self.layers[current_layer_index].remove(current_layer_len - 1);
            self.layers[current_layer_index].push(last_leaf.clone());
            self.layers[current_layer_index].push(last_leaf);
        }
    }

    fn generate_new_layer_parallel(&mut self, current_layer_index: usize) {
        let mut new_layer = self.layers[current_layer_index]
            .par_chunks(2)
            .map(|pair| hash_node(&pair[0], &pair[1]))
            .collect::<Vec<[u8; 32]>>();
        let current_layer_len = self.layers[current_layer_index].len();
        self.create_new_layer(current_layer_index, current_layer_len, &mut new_layer);
    }

    fn generate_new_layer_sequence(&mut self, current_layer_index: usize) {
        let mut new_layer = self.layers[current_layer_index]
            .chunks(2)
            .map(|pair| hash_node(&pair[0], &pair[1]))
            .collect::<Vec<[u8; 32]>>();
        self.create_new_layer(current_layer_index, new_layer.len() * 2, &mut new_layer);
    }

    fn create_new_layer(&mut self,
                        current_layer_index: usize,
                        new_layer_base_capacity: usize,
                        mut new_layer: &mut Vec<[u8; 32]>) {
        let layers_last_index = self.layers.len() - 1;
        if layers_last_index >= current_layer_index + 1 {
            self.layers[current_layer_index + 1].append(&mut new_layer);
        } else {
            self.layers.push(Vec::with_capacity(new_layer_base_capacity));
            self.layers[current_layer_index + 1].append(&mut new_layer);
        }
    }

    pub fn push<Serializable>(&mut self, other: &[Serializable]) where Serializable: Serialize + Clone {
        let serialized_value = self.format.serialize(&other);
        let hashed_value = hash_leaf(&serialized_value);
        if self.builded {
            let leafs_count = self.leafs_count as usize;
            self.recursive_repair_branch(0, leafs_count, hashed_value);
        } else {
            self.layers[0].push(hashed_value);
        }
        self.leafs_count += 1;
    }

    fn recursive_repair_branch(&mut self, layer_index: usize, layer_len: usize, new_node: [u8; 32]) {
        self.print();
        if layer_len > 1 {
            debug!("Layer index: {}. Layer len: {}.", layer_index, layer_len);
            if self.layers[layer_index].len() > layer_len {
                debug!("Layer real len {} > layer len {}", self.layers[layer_index].len(), layer_len);
                debug!("Remove duplicate of parent.");
                let last_node_index = self.layers[layer_index].len() - 1;
                self.layers[layer_index].remove(last_node_index);
                let last_node_index = self.layers[layer_index].len() - 1;
                if layer_index > 0 {
                    debug!("Remove duplicate of parent.");
                    self.layers[layer_index].remove(last_node_index);
                }
            }
            self.layers[layer_index].push(new_node.clone());
            if self.layers[layer_index].len() % 2 != 0 {
                debug!("Add duplicate. {} % 2 = {}", self.layers[layer_index].len(), self.layers[layer_index].len() % 2);
                self.layers[layer_index].push(new_node);
            }
            let last_node_index = self.layers[layer_index].len() - 1;
            let left = self.layers[layer_index][last_node_index];
            let right = self.layers[layer_index][last_node_index - 1];
            let new_node = hash_node(&left, &right);
            self.recursive_repair_branch(layer_index+1, layer_len/2, new_node);
        } else {
            debug!("Layer len < 1");
            self.layers[layer_index].push(new_node);
            let left = self.layers[layer_index][0];
            let right = self.layers[layer_index][1];
            let new_node = hash_node(&left, &right);
            self.layers.push(Vec::with_capacity(layer_len*2));
            self.layers[layer_index+1].push(new_node);
        }
    }

    pub fn print(&self) {
        for layer_index in 0..self.layers.len() {
            println!("Layer size: {}", self.layers[layer_index].len());
            for node_index in 0..self.layers[layer_index].len() {
                print!(" {}", node_index)
            }
            println!("\n");
        }
    }
}

#[cfg(test)]
mod tests {

    extern crate env_logger;
    use super::MerkleTree;
    use super::SerializationFormat;

//    #[test]
//    fn build_tree() {
//        let _ = env_logger::init();
//        let mut merkle_tree_sequence: MerkleTree = MerkleTree::default();
//        merkle_tree_sequence.parallel = false;
//        merkle_tree_sequence.push(&["a"]);
//        merkle_tree_sequence.build();
//        let mut merkle_tree_parallel: MerkleTree = MerkleTree::default();
//        merkle_tree_parallel.push(&["a"]);
//        merkle_tree_parallel.build();
//        assert_eq!(merkle_tree_sequence.layers, merkle_tree_parallel.layers);
//    }
//
//    #[test]
//    #[should_panic(expected = "No leafs in tree!")]
//    fn build_empty_tree() {
//        let _ = env_logger::init();
//        let mut merkle_tree: MerkleTree = MerkleTree::default();
//        merkle_tree.build();
//    }
//
//    #[test]
//    fn build_tree_one_leaf() {
//        let _ = env_logger::init();
//        let mut merkle_tree: MerkleTree = MerkleTree::default();
//        merkle_tree.push(&["a"]);
//        merkle_tree.build();
//        merkle_tree.print();
//        assert_eq!(merkle_tree.leafs_count, 1);
//    }
//
//    #[test]
//    fn build_tree_two_leaf() {
//        let _ = env_logger::init();
//        let mut merkle_tree: MerkleTree = MerkleTree::default();
//        merkle_tree.push(&["a"]);
//        merkle_tree.push(&["a"]);
//        merkle_tree.build();
//        assert_eq!(merkle_tree.leafs_count, 2);
//    }
//
//    #[test]
//    fn build_tree_three_leaf() {
//        let _ = env_logger::init();
//        let mut merkle_tree: MerkleTree = MerkleTree::default();
//        merkle_tree.push(&["a"]);
//        merkle_tree.push(&["a"]);
//        merkle_tree.push(&["a"]);
//        merkle_tree.build();
//        assert_eq!(merkle_tree.leafs_count, 3);
//    }
//
//    #[test]
//    fn build_tree_four_leaf() {
//        let _ = env_logger::init();
//        let mut merkle_tree: MerkleTree = MerkleTree::default();
//        merkle_tree.push(&["a"]);
//        merkle_tree.push(&["a"]);
//        merkle_tree.push(&["a"]);
//        merkle_tree.push(&["a"]);
//        merkle_tree.build();
//        merkle_tree.print();
//        assert_eq!(merkle_tree.leafs_count, 4);
//    }

//    #[test]
//    fn build_tree_from_leafs() {
//        let _ = env_logger::init();
//        let mut merkle_tree: MerkleTree = MerkleTree::from(&mut ["a", "b", "c", "d"], SerializationFormat::Json);
//        merkle_tree.print();
//        merkle_tree.build();
//        merkle_tree.print();
//        assert_eq!(merkle_tree.leafs_count, 4);
//    }
//
    #[test]
    fn repair_branch() {
        let _ = env_logger::init();
        let mut merkle_tree: MerkleTree = MerkleTree::from(&mut ["a", "b", "c", "d"], SerializationFormat::Json);
        merkle_tree.print();
        merkle_tree.build();
        merkle_tree.print();
        merkle_tree.push(&["e"]);
        merkle_tree.print();
        merkle_tree.push(&["f"]);
        merkle_tree.print();
        merkle_tree.push(&["f"]);
        merkle_tree.print();
        assert_eq!(merkle_tree.leafs_count, 7);
    }

}
