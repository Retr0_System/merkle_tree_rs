extern crate merkle_tree;
extern crate serde;
extern crate bincode;
#[macro_use]
extern crate serde_derive;
extern crate hex_slice;
#[macro_use]
extern crate log;
extern crate env_logger;

use merkle_tree::{ SerializationFormat};
use std::collections::HashMap;
use hex_slice::AsHex;

#[derive(Debug, Serialize, Clone)]
pub struct TestStructure {
    data_part_one: String,
    data_part_two: u32,
    data_part_three: HashMap<String, u8>,
}

impl Default for TestStructure {
    fn default() -> TestStructure {
        let mut data_part_three = HashMap::new();
        data_part_three.insert(String::from("test"), 127);
        TestStructure {
            data_part_one: String::from("Test string"),
            data_part_two: 800,
            data_part_three: data_part_three,
        }
    }
}

fn main() {
    let env = env_logger::init();
//    debug!("Result of serialization TestStructure to JSON: {:#X}",
//           check_serialization(SerializationFormat::Json, &TestStructure::default()).as_hex());
}
